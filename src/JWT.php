<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth;

use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Carbon;
use Shizzen\JWTAuth\Contracts\JWTSubject;
use Shizzen\JWTAuth\Exceptions\BlacklistedTokenException;
use Shizzen\JWTAuth\Exceptions\ExpiredTokenException;
use Shizzen\JWTAuth\Exceptions\InvalidClaimException;
use Shizzen\JWTAuth\Exceptions\MissingClaimException;
use Shizzen\JWTAuth\Exceptions\UnknownSubjectException;
use Stringable;

class JWT implements ArrayAccess, Stringable, Arrayable, Jsonable
{
    protected JWTManager $manager;

    /**
     * @var Claim[]
     */
    protected array $payload;

    /**
     * @param array<string, mixed> $payload
     */
    public function __construct(JWTManager $manager, array $payload = [])
    {
        $this->setManager($manager);
        $this->setPayload($payload);
    }

    /**
     * Get the current manager instance.
     */
    public function getManager(): JWTManager
    {
        return $this->manager;
    }

    /**
     * Set the current manager instance.
     * 
     * @return $this
     */
    public function setManager(JWTManager $manager): static
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return Claim[]
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @return array<string, int|float|string|array|null>
     */
    public function getRawPayload(): array
    {
        $rawPayload = [];

        foreach ($this->getPayload() as $claim) {
            $rawPayload[$claim->getName()] = $claim->getValue(true);
        }

        return $rawPayload;
    }

    /**
     * @param array<string, mixed> $claims
     * 
     * @return $this
     */
    public function setPayload(array $payload): static
    {
        foreach ($payload as $claimName => $claimValue) {
            $this->setClaim($claimName, $claimValue);
        }

        return $this;
    }

    /**
     * @return string[]
     */
    public function getClaimNames(): array
    {
        return array_map(
            fn(Claim $claim): string => $claim->getName(),
            $this->getPayload()
        );
    }

    public function getClaim(string $claimName): ?Claim
    {
        return $this[$claimName];
    }

    /**
     * Check whether the JWT contains a given clain.
     */
    public function hasClaim(string $claimName): bool
    {
        return isset($this[$claimName]);
    }

    /**
     * @return $this
     */
    public function setClaim(string $claimName, mixed $claimValue): static
    {
        $this[$claimName] = $claimValue;

        return $this;
    }

    /**
     * @return $this
     */
    public function unsetClaim(string $claimName): static
    {
        unset($this[$claimName]);

        return $this;
    }

    /**
     * Guess a value which uniquely identify this JWT.
     */
    public function getId(): string
    {
        return $this->getClaim(Claim::JWT_ID)?->getValue() ?: (string) $this;
    }

    /**
     * Get the remaining seconds before token expiration (null if infinite).
     */
    public function getRemainingTime(): ?int
    {
        if (! $expiration = $this->getClaim(Claim::EXPIRATION)) {
            return null;
        }

        return Carbon::now()->getTimestamp() - $expiration->getValue();
    }

    /**
     * Get the remaining minutes before token expiration (null if infinite).
     */
    public function getTTL(): ?int
    {
        $seconds = $this->getRemainingTime();

        return is_null($seconds) ? null : intdiv($seconds, 60);
    }

    public function getSubject(): JWTSubject
    {
        return $this->getManager()->jwtToSubject($this);
    }

    /**
     * Invalidate a token (add it to the blacklist).
     * 
     * @return $this
     */
    public function invalidate(): static
    {
        $this->getManager()->invalidate($this);

        return $this;
    }

    /**
     * Refresh the JWT and return a new one.
     * 
     * @param array<string, mixed> $customClaims The claims to merge with the refreshed token (they take priority)
     * 
     * @throws InvalidClaimException
     */
    public function refresh(): static
    {
        return $this->getManager()->refresh($this);
    }

    /**
     * Check that the token is valid.
     * 
     * @throws BlacklistedTokenException
     * @throws ExpiredTokenException
     * @throws UnknownSubjectException
     * @throws MissingClaimException
     */
    public function check(): void
    {
        $this->getManager()->check($this);
    }

    /**
     * {@inheritdoc}
     * 
     * @return array<string, int|float|string|array|null>
     */
    public function toArray(): array
    {
        return $this->getRawPayload();
    }

    /**
     * {@inheritdoc}
     */
    public function toJson($options = 0): string
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * @param string $offset
     */
    public function offsetExists(mixed $offset): bool
    {
        foreach ($this->claims as $claim) {
            if ($claim->getName() === $offset) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * @param string $offset
     * 
     * @return Claim|null
     */
    public function offsetGet(mixed $offset): ?Claim
    {
        foreach ($this->claims as $claim) {
            if ($claim->getName() === $offset) {
                return $claim;
            }
        }
        
        return null;
    }

    /**
     * @param string $offset
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        foreach ($this->claims as $claim) {
            if ($claim->getName() === $offset) {
                $claim->setValue($value);
                return;
            }
        }
        
        $this->claims[] = new Claim($offset, $value);
    }

    /**
     * @param string $offset
     */
    public function offsetUnset(mixed $offset): void
    {
        foreach ($this->claims as $index => $claim) {
            if ($claim->getName() === $offset) {
                unset($this->claims[$index]);
                break;
            }
        }
    }

    public function __toString(): string
    {
        return $this->getManager()->encode($this);
    }

    public function __clone(): void
    {
        $this->claims = array_map(
            fn(Claim $claim): Claim => clone $claim,
            $this->claims
        );
    }
}
