<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Exceptions;

use Throwable;

class InvalidClaimException extends JWTException
{
    public function __construct(
        string $claimName,
        string $message,
        int $code = 0,
        ?Throwable $previous = null
    ) {
        parent::__construct(
            sprintf('Invalid value provided for claim [%s]: %s', $claimName, $message),
            $code,
            $previous
        );
    }
}
