<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Exceptions;

use Exception;

abstract class JWTException extends Exception {}
