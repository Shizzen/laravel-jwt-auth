<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Exceptions;

use Throwable;

class MissingTokenException extends JWTException
{
    public function __construct(string $guardName, int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct(
            sprintf('There is no JWT bound to the [%s] guard', $guardName),
            $code,
            $previous
        );
    }
}
