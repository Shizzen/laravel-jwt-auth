<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Exceptions;

class ExpiredTokenException extends JWTException
{
    /**
     * {@inheritdoc}
     */
    protected string $message = 'The token has expired';
}
