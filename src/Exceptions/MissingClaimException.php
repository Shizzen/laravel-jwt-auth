<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Exceptions;

use Throwable;

class MissingClaimException extends JWTException
{
    public function __construct(string $claimName, int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct(
            sprintf('Missing claim [%s]', $claimName),
            $code,
            $previous
        );
    }
}
