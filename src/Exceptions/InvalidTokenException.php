<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Exceptions;

class InvalidTokenException extends JWTException {}
