<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Exceptions;

class UnknownSubjectException extends JWTException
{
    /**
     * {@inheritdoc}
     */
    protected string $message = 'Unknown subject';
}
