<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Exceptions;

class BlacklistedTokenException extends JWTException
{
    /**
     * {@inheritdoc}
     */
    protected string $message = 'The token has been blacklisted';
}
