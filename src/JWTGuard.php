<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Events\Dispatcher as EventDispatcher;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Traits\Macroable;
use ReflectionProperty;
use Shizzen\JWTAuth\Contracts\JWTSubject;
use Shizzen\JWTAuth\Events\JWTAttempting;
use Shizzen\JWTAuth\Events\JWTFailed;
use Shizzen\JWTAuth\Events\JWTLogin;
use Shizzen\JWTAuth\Events\JWTLogout;
use Shizzen\JWTAuth\Events\JWTRefresh;
use Shizzen\JWTAuth\Exceptions\MissingTokenException;
use TypeError;

class JWTGuard implements Guard, Responsable
{
    use Macroable;

    protected string $guardName;

    /**
     * The authenticated JWT
     */
    protected JWT $jwt;

    protected JWTManager $manager;

    protected Request $request;

    protected ?CookieJar $cookieJar;

    protected ?EventDispatcher $eventDispatcher;

    public function __construct(
        string $guardName,
        JWTManager $manager,
        Request $request,
        ?CookieJar $cookieJar,
        ?EventDispatcher $eventDispatcher
    ) {
        $this->setGuardName($guardName);
        $this->setManager($manager);
        $this->setRequest($request);
        $this->setCookieJar($cookieJar);
        $this->setEventDispatcher($eventDispatcher);
    }

    public function getGuardName(): string
    {
        return $this->guardName;
    }

    /**
     * @return $this
     */
    public function setGuardName(string $guardName): static
    {
        $this->guardName = $guardName;

        return $this;
    }
    
    /**
     * Get the current manager instance.
     */
    public function getManager(): JWTManager
    {
        return $this->manager;
    }

    /**
     * Set the current manager instance.
     * 
     * @return $this
     */
    public function setManager(JWTManager $manager): static
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get the current request instance.
     */
    public function getEventDispatcher(): ?EventDispatcher
    {
        return $this->eventDispatcher;
    }

    /**
     * Set the current request instance.
     * 
     * @return $this
     */
    public function setEventDispatcher(?EventDispatcher $eventDispatcher): static
    {
        $this->eventDispatcher = $eventDispatcher;

        return $this;
    }

    /**
     * Get the current request instance.
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * Set the current request instance and reset the cached JWT.
     * 
     * @return $this
     */
    public function setRequest(Request $request): static
    {
        $this->request = $request;
        $this->jwt = null;

        return $this;
    }

    public function getCookieJar(): ?CookieJar
    {
        return $this->cookieJar;
    }

    /**
     * @return $this
     */
    public function setCookieJar(?CookieJar $cookieJar): static
    {
        $this->cookieJar = $cookieJar;

        return $this;
    }

    /**
     * Get the name of the cookie used to store the JWT.
     */
    public function getCookieName(): string
    {
        return sprintf('remember_%s_%s', $this->getGuardName(), sha1(static::class));
    }

    public function getCookie(): ?string
    {
        return $this->getRequest()->cookie($this->getCookieName());
    }

    /**
     * @return $this
     */
    public function setCookie(): static
    {
        $jwt = $this->getJWT();
        $ttl = $jwt->getTTL();

        if (is_null($ttl)) {
            $this->getCookieJar()?->forever(
                $this->getCookieName(),
                (string) $jwt
            );
        }
        else {
            $this->getCookieJar()?->queue(
                $this->getCookieName(),
                (string) $jwt,
                $ttl
            );
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function unsetCookie(): static
    {
        $this->getCookieJar()?->expire($this->getCookieName());

        return $this;
    }

    public function getTTL(): ?int
    {
        return $this->getManager()->getTTL();
    }

    /**
     * Set the JWT ttl.
     * 
     * @return $this
     */
    public function setTTL(?int $ttl): static
    {
        $this->getManager()->setTTL($ttl);

        return $this;
    }

    /**
     * Get the user provider used by the guard.
     */
    public function getUserProvider(): UserProvider
    {
        return $this->getManager()->getUserProvider();
    }

    /**
     * Get the current JWT instance.
     */
    public function getJWT(): ?JWT
    {
        $jwtProperty = new ReflectionProperty(static::class, 'jwt');
        
        if (! $jwtProperty->isInitialized($this)) {
            $this->setJWT($this->fromRequest());
        }
        
        return $this->jwt;
    }

    /**
     * Set the current JWT instance.
     * 
     * @return $this
     */
    public function setJWT(?JWT $jwt): static
    {
        $this->jwt = $jwt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function check(): bool
    {
        return ! is_null($this->user());
    }

    /**
     * {@inheritdoc}
     */
    public function hasUser(): bool
    {
        return $this->check();
    }

    /**
     * {@inheritdoc}
     */
    public function guest(): bool
    {
        return ! $this->check();
    }

    /**
     * {@inheritdoc}
     */
    public function user(): ?JWTSubject
    {
        return $this->getJWT()?->getSubject();
    }

    /**
     * {@inheritdoc}
     */
    public function id(): int|string|null
    {
        return $this->user()?->getAuthIdentifier();
    }

    /**
     * {@inheritdoc}
     */
    public function validate(array $credentials = []): bool
    {
        return $this->attempt($credentials, false);
    }

    /**
     * {@inheritdoc}
     */
    public function attempt(array $credentials = [], $remember = false): bool
    {
        $this->fireAttemptEvent($credentials);

        $userProvider = $this->getUserProvider();

        if (! $user = $userProvider->retrieveByCredentials($credentials)) {
            $this->fireFailedEvent(null, $credentials);

            return false;
        }

        if ($userProvider->validateCredentials($user, $credentials)) {
            $this->login($user, $remember);

            return true;
        }

        $this->fireFailedEvent($user, $credentials);

        return false;
    }

    /**
     * Log a user into the application, optionally remembered thanks to a cookie.
     */
    public function login(JWTSubject $user, bool $remember = false): void
    {
        $this->setUser($user);

        $this->fireLoginEvent($user, $this->getJWT());

        if ($remember) {
            $this->setCookie();
        }
    }

    /**
     * Retrieve a user and then log him into the application, optionally remembered thanks to a cookie.
     */
    public function loginUsingId(int|string $id, bool $remember = false): ?JWTSubject
    {
        if (! $user = $this->getUserProvider()->retrieveById($id)) {
            return null;
        }

        $this->login($user, $remember);

        return $user;
    }

    /**
     * {@inheritdoc}
     *
     * @param  JWTSubject  $user
     */
    public function setUser(Authenticatable $user): void
    {
        $jwt = $this->getManager()->subjectToJwt($user);

        $this->setJWT($jwt);
    }

    protected function fireAttemptEvent(array $credentials): void
    {
        $this->getEventDispatcher()?->dispatch(
            new JWTAttempting($this->getGuardName(), $credentials)
        );
    }

    protected function fireFailedEvent(?JWTSubject $subject, array $credentials): void
    {
        $this->getEventDispatcher()?->dispatch(
            new JWTFailed($this->getGuardName(), $subject, $credentials)
        );
    }

    protected function fireLoginEvent(JWTSubject $subject, JWT $jwt): void
    {
        $this->getEventDispatcher()?->dispatch(
            new JWTLogin($this->getGuardName(), $subject, $jwt)
        );
    }

    protected function fireLogoutEvent(JWTSubject $subject, JWT $jwt): void
    {
        $this->getEventDispatcher()?->dispatch(
            new JWTLogout($this->getGuardName(), $subject, $jwt)
        );
    }

    protected function fireRefreshEvent(JWTSubject $subject, JWT $oldJwt, JWT $newJwt): void
    {
        $this->getEventDispatcher()?->dispatch(
            new JWTRefresh($this->getGuardName(), $subject, $oldJwt, $newJwt)
        );
    }

    /**
     * Logout the user, thus invalidating the token.
     */
    public function logout(): void
    {
        if (! $jwt = $this->getJWT()) {
            return;
        }

        $this->getManager()->invalidate($jwt);

        $this->setJWT(null);

        $this->unsetCookie();

        $this->fireLogoutEvent($jwt->getSubject(), $jwt);
    }

    /**
     * Refresh the token.
     */
    public function refresh(): void
    {
        $oldJwt = $this->getJWT();
        $newJwt = $oldJwt->refresh();

        $this->setJWT($newJwt);

        $this->fireRefreshEvent($this->user(), $oldJwt, $newJwt);
    }

    /**
     * Try to get a token from the current request.
     */
    protected function fromRequest(): ?JWT
    {
        $manager = $this->getManager();

        $request = $this->getRequest();

        if ($bearerToken = $request->bearerToken()) {
            return $manager->decode($bearerToken);
        }
        if ($cookie = $request->cookie($this->getCookieName())) {
            return $manager->decode($cookie);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     * 
     * @throws MissingTokenException If no JWT bound to the guard
     */
    public function toResponse($request): JsonResponse
    {
        if (! $jwt = $this->getJWT()) {
            throw new MissingTokenException($this->getGuardName());
        }

        $rawJwt = (string) $jwt;

        $response = new JsonResponse([
            'token_type' => 'Bearer',
            'access_token' => $rawJwt,
            'expires_in' => $jwt->getTTL(),
        ]);

        $response->header('Authorization', 'Bearer ' . $rawJwt);

        return $response;
    }
}
