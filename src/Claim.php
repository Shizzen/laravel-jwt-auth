<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Shizzen\JWTAuth\Exceptions\InvalidClaimException;
use Stringable;

/**
 * @link https://www.rfc-editor.org/rfc/rfc7519.html#section-4
 */
class Claim
{
    public const AUDIENCE = 'aud';
    public const EXPIRATION = 'exp';
    public const HASHED_SUBJECT = 'hsu';
    public const ISSUED_AT = 'iat';
    public const ISSUER = 'iss';
    public const JWT_ID = 'jti';
    public const NOT_BEFORE = 'nbf';
    public const SUBJECT = 'sub';

    /**
     * @var array<string, callable(mixed, string): void>
     */
    protected static array $customValidators = [];

    /**
     * The claim's value
     */
    protected mixed $value;

    /**
     * @throws InvalidClaimException
     */
    public static function validateSubject(mixed $claimValue, string $claimName): void
    {
        if (! is_string($claimValue)) {
            throw new InvalidClaimException($claimName, 'Must be a string.');
        }
    }

    /**
     * @throws InvalidClaimException
     */
    public static function validateExpiration(mixed $claimValue, string $claimName): void
    {
        if (! is_int($claimValue)) {
            throw new InvalidClaimException($claimName, 'Must be an int.');
        }
    }

    /**
     * @throws InvalidClaimException
     */
    public static function validateIssuer(mixed $claimValue, string $claimName): void
    {
        if (! is_string($claimValue)) {
            throw new InvalidClaimException($claimName, 'Must be a string.');
        }
    }

    /**
     * @throws InvalidClaimException
     */
    public static function validateIssuedAt(mixed $claimValue, string $claimName): void
    {
        if (! is_int($claimValue)) {
            throw new InvalidClaimException($claimName, 'Must be an int.');
        }
    }

    /**
     * @throws InvalidClaimException
     */
    public static function validateNotBefore(mixed $claimValue, string $claimName): void
    {
        if (! is_int($claimValue)) {
            throw new InvalidClaimException($claimName, 'Must be an int.');
        }
    }

    /**
     * @param string $name The claim's name
     * 
     * @throws InvalidClaimException
     */
    public function __construct(
        protected readonly string $name,
        mixed $value
    ) {
        $this->setValue($value);
    }

    /**
     * Get the claim's value and optionally format it.
     */
    public function getValue(bool $formatted = false): mixed
    {
        if (! $formatted) {
            return $this->value;
        }
        elseif ($this->value instanceof Arrayable) {
            return $this->value->toArray();
        }
        elseif ($this->value instanceof Jsonable) {
            return $this->value->toJson();
        }
        elseif ($this->value instanceof Stringable) {
            return (string) $this->value;
        }
        else {
            return $this->value;
        }
    }

    /**
     * Set the claim's value after getting validated.
     * 
     * @throws InvalidClaimException
     */
    public function setValue(mixed $value): static
    {
        $this->validate($value);

        $this->value = $value;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Guess whether the claim's value should be refreshed.
     */
    public function refreshable(): bool
    {
        return in_array($this->getName(), [
            static::JWT_ID,
            static::EXPIRATION,
            static::ISSUED_AT,
            static::NOT_BEFORE,
        ]);
    }

    /**
     * Validate the claim's value
     * 
     * Verify the $value type is JWT-compliant and some custom conditions depending on the claim's name
     * 
     * @throws InvalidClaimException
     */
    public function validate(mixed $value): void {
        $claimName = $this->getName();

        if (is_object($value)) {
            $isCastable = $value instanceof Arrayable
                || $value instanceof Jsonable
                || $value instanceof Stringable;

            if (! $isCastable) {
                throw new InvalidClaimException(
                    $claimName,
                    sprintf('Wrong type [%s]', get_class($value))
                );
            }
        }

        $customValidator = static::getCustomValidator($claimName, true);

        $customValidator($value, $claimName);
    }

    public static function hasCustomValidator(string $claimName): bool
    {
        return array_key_exists($claimName, static::$customValidators);
    }

    /**
     * @param bool $fallback Whether a 'always true' callback should be returned instead of null
     * 
     * @return null|callable(mixed, string): void
     */
    public static function getCustomValidator(string $claimName, bool $fallback = false): ?callable
    {
        $fallbackValue = ! $fallback ? null : fn(mixed $claimValue, string $claimName): bool => true;

        return static::$customValidators[$claimName] ?? $fallbackValue;
    }

    /**
     * @param callable(mixed, string): void $callback
     */
    public static function setCustomValidator(string $claimName, callable $callback): void
    {
        static::$customValidators[$claimName] = $callback;
    }

    public static function unsetCustomValidator(string $claimName): void
    {
        unset(static::$customValidators[$claimName]);
    }

    public static function flushCustomValidators(): void
    {
        static::$customValidators = [];
    }

    public function __clone(): void
    {
        if (is_object($this->value)) {
            $this->value = clone $this->value;
        }
    }
}
