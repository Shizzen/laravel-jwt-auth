<?php

namespace Shizzen\JWTAuth\JWTProviders;

use Exception;
use Illuminate\Support\Collection;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer;
use ReflectionClass;
use Shizzen\JWTAuth\Exceptions\JWTException;
use Shizzen\JWTAuth\Exceptions\TokenInvalidException;

class Lcobucci extends Provider
{
    /**
     * The Builder instance.
     */
    protected Builder $builder;

    /**
     * The Parser instance.
     */
    protected Parser $parser;

    /**
     * The Signer instance.
     */
    protected Signer $signer;

    /**
     * Constructor.
     */
    public function __construct(
        Builder $builder,
        Parser $parser,
        string $secret,
        string $algo,
        array $keys
    ) {
        parent::__construct($secret, $algo, $keys);

        $this->builder = $builder;
        $this->parser = $parser;
        $this->signer = $this->getSigner();
    }

    /**
     * Algorithms that this provider supports.
     *
     * @var array
     */
    protected $algorithms = [
        'HS256' => Signer\Hmac\Sha256::class,
        'HS384' => Signer\Hmac\Sha384::class,
        'HS512' => Signer\Hmac\Sha512::class,
        'RS256' => Signer\Rsa\Sha256::class,
        'RS384' => Signer\Rsa\Sha384::class,
        'RS512' => Signer\Rsa\Sha512::class,
        'ES256' => Signer\Ecdsa\Sha256::class,
        'ES384' => Signer\Ecdsa\Sha384::class,
        'ES512' => Signer\Ecdsa\Sha512::class,
    ];

    /**
     * Create a JSON Web Token.
     *
     * @throws \Shizzen\JWTAuth\Exceptions\JWTException
     */
    public function encode(array $payload): string
    {
        // Remove the signature on the builder instance first.
        $this->builder->unsign();

        $signingKey = $this->getSigningKey();

        try {
            foreach ($payload as $key => $value) {
                $this->builder->set($key, $value);
            }
            $this->builder->sign($this->signer, $signingKey);
        } catch (Exception $e) {
            throw new JWTException('Could not create token: '.$e->getMessage(), $e->getCode(), $e);
        }

        return (string) $this->builder->getToken($this->signer, $signingKey);
    }

    /**
     * Decode a JSON Web Token.
     *
     * @throws \Shizzen\JWTAuth\Exceptions\JWTException
     */
    public function decode(string $token): array
    {
        try {
            $jwt = $this->parser->parse($token);
        } catch (Exception $e) {
            throw new TokenInvalidException(
                'Could not decode token: '.$e->getMessage(),
                $e->getCode(),
                $e
            );
        }

        if (! $jwt->verify($this->signer, $this->getVerificationKey())) {
            throw new TokenInvalidException('Token Signature could not be verified.');
        }

        return Collection::make($jwt->getClaims())
            ->map(fn ($claim) => is_object($claim) ? $claim->getValue() : $claim)
            ->toArray();
    }

    /**
     * Get the Signer instance.
     *
     * @throws \Shizzen\JWTAuth\Exceptions\JWTException
     */
    protected function getSigner(): Signer
    {
        if (! array_key_exists($this->algo, $this->algorithms)) {
            throw new JWTException('The given algorithm could not be found');
        }

        return new $this->algorithms[$this->algo]();
    }

    /**
     * {@inheritdoc}
     */
    protected function isAsymmetric(): bool
    {
        $reflect = new ReflectionClass($this->signer);

        return $reflect->isSubclassOf(Signer\Rsa::class)
            || $reflect->isSubclassOf(Signer\Ecdsa::class);
    }

    /**
     * {@inheritdoc}
     */
    protected function getSigningKey()
    {
        return $this->isAsymmetric()
            ? (new Signer\Keychain())->getPrivateKey($this->getPrivateKey(), $this->getPassphrase())
            : new Signer\Key($this->getSecret());
    }

    /**
     * {@inheritdoc}
     */
    protected function getVerificationKey()
    {
        return $this->isAsymmetric()
            ? (new Signer\Keychain())->getPublicKey($this->getPublicKey())
            : new Signer\Key($this->getSecret());
    }
}
