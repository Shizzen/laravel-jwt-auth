<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\JWTProviders;

use Illuminate\Support\Arr;
use Shizzen\JWTAuth\Contracts\JWTProvider;

abstract class Provider implements JWTProvider
{
    /**
     * The secret.
     */
    protected string $secret;

    /**
     * The array of keys.
     */
    protected array $keys;

    /**
     * The used algorithm.
     */
    protected string $algo;

    /**
     * Constructor.
     */
    public function __construct(string $secret, string $algo, array $keys)
    {
        $this->setSecret($secret);
        $this->setAlgo($algo);
        $this->setKeys($keys);
    }

    /**
     * Get the algorithm used to sign the token.
     */
    public function getAlgo(): string
    {
        return $this->algo;
    }

    /**
     * Set the algorithm used to sign the token.
     * 
     * @return $this
     */
    public function setAlgo(string $algo): static
    {
        $this->algo = $algo;

        return $this;
    }

    /**
     * Get the secret used to sign the token.
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * Set the secret used to sign the token.
     * 
     * @return $this
     */
    public function setSecret(string $secret): static
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Get the array of keys used to sign tokens
     * with an asymmetric algorithm.
     */
    public function getKeys(): array
    {
        return $this->keys;
    }

    /**
     * Set the keys used to sign the token.
     * 
     * @return $this
     */
    public function setKeys(array $keys): static
    {
        $this->keys = $keys;

        return $this;
    }

    /**
     * Get the public key used to sign tokens
     * with an asymmetric algorithm.
     *
     * @return resource|string
     */
    public function getPublicKey()
    {
        return Arr::get($this->keys, 'public');
    }

    /**
     * Get the private key used to sign tokens
     * with an asymmetric algorithm.
     *
     * @return resource|string
     */
    public function getPrivateKey()
    {
        return Arr::get($this->keys, 'private');
    }

    /**
     * Get the passphrase used to sign tokens
     * with an asymmetric algorithm.
     */
    public function getPassphrase(): ?string
    {
        return Arr::get($this->keys, 'passphrase');
    }

    /**
     * Get the key used to sign the tokens.
     *
     * @return resource|string
     */
    protected function getSigningKey()
    {
        return $this->isAsymmetric()
            ? $this->getPrivateKey()
            : $this->getSecret();
    }

    /**
     * Get the key used to verify the tokens.
     *
     * @return resource|string
     */
    protected function getVerificationKey()
    {
        return $this->isAsymmetric()
            ? $this->getPublicKey()
            : $this->getSecret();
    }

    /**
     * Determine if the algorithm is asymmetric, and thus
     * requires a public/private key combo.
     */
    abstract protected function isAsymmetric(): bool;
}
