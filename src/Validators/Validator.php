<?php

namespace Shizzen\JWTAuth\Validators;

use Shizzen\JWTAuth\Exceptions\JWTException;
use Shizzen\JWTAuth\Exceptions\TokenInvalidException;

abstract class Validator
{
    /**
     * Helper function to return a boolean.
     */
    public static function isValid(...$args): bool
    {
        try {
            forward_static_call('static::check', ...$args);
        } catch (JWTException $e) {
            return false;
        }

        return true;
    }

    /**
     * Validation failed.
     */
    public static function throwFailed(string $message = 'Invalid'): void
    {
        throw new TokenInvalidException($message);
    }
}
