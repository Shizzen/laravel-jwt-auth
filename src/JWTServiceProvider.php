<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth;

use Illuminate\Auth\AuthManager;
use Illuminate\Container\Container;
use Illuminate\Contracts\Cache\Factory as CacheManager;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Contracts\Events\Dispatcher as EventDispatcher;
use Illuminate\Cookie\CookieJar;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Shizzen\JWTAuth\Blacklist;
use Shizzen\JWTAuth\Console\JWTGenerateSecretCommand;
use Shizzen\JWTAuth\Contracts\JWTProvider;

class JWTServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function register(): void
    {
        $this->registerGuard()
            ->registerJWTManager()
            ->registerBlacklist();
    }

    public function boot(AuthManager $authManager): void
    {
        $configPath = realpath(__DIR__.'/../../config/jwt.php');

        $this->mergeConfigFrom($configPath, 'jwt');

        if ($this->app->runningInConsole()) {
            $this->publishes([$configPath => config_path('jwt.php')], 'config');

            $this->commands([
                JWTGenerateSecretCommand::class,
            ]);
        }

        Claim::setCustomValidator(Claim::SUBJECT, [Claim::class, 'validateSubject']);
        Claim::setCustomValidator(Claim::EXPIRATION, [Claim::class, 'validateExpiration']);
        Claim::setCustomValidator(Claim::ISSUER, [Claim::class, 'validateIssuer']);
        Claim::setCustomValidator(Claim::ISSUED_AT, [Claim::class, 'validateIssuedAt']);
        Claim::setCustomValidator(Claim::NOT_BEFORE, [Claim::class, 'validateNotBefore']);

        $authManager->extend('jwt', function (Container $container, string $guardName, array $guardConfig) use ($authManager): JWTGuard {
            /** @var Config */
            $config = $container->make(Config::class);

            /** @var class-string<JWTProvider> */
            $jwtProviderClass = Arr::get($guardConfig, 'jwt.provider', $config->get('jwt.provider'));

            /** @var JWTProvider */
            $jwtProvider = $container->make($jwtProviderClass, [
                'secret' => Arr::get($guardConfig, 'jwt.secret', $config->get('jwt.secret')),
                'algo' => Arr::get($guardConfig, 'jwt.algo', $config->get('jwt.algo')),
                'keys' => Arr::get($guardConfig, 'jwt.keys', $config->get('jwt.keys')),
            ]);

            /** @var class-string<JWTManager> */
            $managerClass = Arr::get($guardConfig, 'jwt.manager', $config->get('jwt.manager'));

            /** @var JWTManager */
            $manager = $container->make($managerClass, [
                'jwtProvider' => $jwtProvider,
                'userProvider' => $authManager->createUserProvider($guardConfig['provider']),
                'ttl' => Arr::get($guardConfig, 'jwt.ttl', $config->get('jwt.ttl')),
                'requiredClaims' => Arr::get($guardConfig, 'jwt.required_claims', $config->get('jwt.required_claims')),
            ]);

            /** @var class-string<JWTGuard> */
            $guardClass = Arr::get($guardConfig, 'jwt.guard', $config->get('jwt.guard'));

            /** @var CookieJar|null */
            $cookieJar = Arr::get($guardConfig, 'jwt.use_cookies', $config->get('jwt.use_cookies'))
                ? $container->make(CookieJar::class)
                : null;

            /** @var EventDispatcher|null */
            $eventDispatcher = Arr::get($guardConfig, 'jwt.use_events', $config->get('jwt.use_events'))
                ? $container->make(EventDispatcher::class)
                : null;

            return $container->make($guardClass, compact(
                'guardName',
                'manager',
                'cookieJar',
                'eventDispatcher',
            ));
        });
    }

    /**
     * Register the bindings for the JSON Web Token guard.
     * 
     * @return $this
     */
    protected function registerGuard(): static
    {
        $this->app->resolving(JWTGuard::class, function (JWTGuard $guard, Container $container): void {
            $container->refresh('request', $guard, 'setRequest');
        });

        return $this;
    }

    /**
     * Register the bindings for the JWT Manager.
     *
     * @return $this
     */
    protected function registerJWTManager(): static
    {
        $this->app->resolving(JWTManager::class, function (JWTManager $manager, Container $container): void {
            $container->refresh(Blacklist::class, $manager, 'setBlacklist');
        });

        return $this;
    }

    /**
     * Register the bindings for the Blacklist.
     *
     * @return $this
     */
    protected function registerBlacklist(): static
    {
        $this->app->singleton(Blacklist::class, function (Container $container): Blacklist {
            /** @var Config */
            $config = $container->make(Config::class);

            /** @var CacheManager */
            $cacheManager = $container->make(CacheManager::class);

            return new Blacklist(
                $cacheManager->store($config->get('jwt.blacklist.cache')),
                $config->get('jwt.blacklist.grace_period')
            );
        });

        return $this;
    }
}
