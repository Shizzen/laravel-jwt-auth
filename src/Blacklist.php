<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth;

use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Support\Carbon;

class Blacklist
{
    /**
     * The storage.
     */
    protected Cache $cache;

    /**
     * The grace period when a token is blacklisted but still accepted. In seconds.
     */
    protected int $gracePeriod;

    /**
     * Constructor.
     */
    public function __construct(Cache $cache, int $gracePeriod = 0)
    {
        $this->setCache($cache);
        $this->setGracePeriod($gracePeriod);
    }

    public function getCache(): Cache
    {
        return $this->cache;
    }

    /**
     * @return $this
     */
    public function setCache(Cache $cache): static
    {
        $this->cache = $cache;

        return $this;
    }

    /**
     * Get the grace period.
     */
    public function getGracePeriod(): int
    {
        return $this->gracePeriod;
    }

    /**
     * Set the grace period.
     * 
     * @return $this
     */
    public function setGracePeriod(int $gracePeriod): static
    {
        $this->gracePeriod = $gracePeriod;

        return $this;
    }

    /**
     * Get the timestamp when the blacklist comes into effect
     * This defaults to immediate (0 seconds).
     */
    protected function getGraceTimestamp(): int
    {
        return Carbon::now()
            ->addSeconds($this->getGracePeriod())
            ->getTimestamp();
    }

    /**
     * Add the token (jti claim) to the blacklist.
     */
    public function add(JWT $jwt): void
    {
        $this->getCache()->forever($jwt->getId(), $this->getGraceTimestamp());
    }

    /**
     * Determine whether the token has been blacklisted and not graced.
     */
    public function has(JWT $jwt): bool
    {
        $graceTimestamp = $this->getCache()->get($jwt->getId());

        return !is_null($graceTimestamp) && $graceTimestamp < $this->getGraceTimestamp();
    }

    /**
     * Remove the token from the blacklist.
     */
    public function forget(JWT $jwt): void
    {
        $this->getCache()->forget($jwt->getId());
    }

    /**
     * Remove all tokens from the blacklist.
     */
    public function flush(): void
    {
        $this->getCache()->getStore()->flush();
    }
}
