<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Events;

use Illuminate\Queue\SerializesModels;
use Shizzen\JWTAuth\Contracts\JWTSubject;
use Shizzen\JWTAuth\JWT;

class JWTLogin extends JWTEvent
{
    use SerializesModels;

    /**
     * {@inheritdoc}
     * 
     * @param JWTSubject $subject The user
     */
    public function __construct(
        string $guard,
        public readonly JWTSubject $subject,
        public readonly JWT $jwt
    ) {
        parent::__construct($guard);
    }
}
