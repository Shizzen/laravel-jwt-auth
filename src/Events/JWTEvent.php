<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Events;

use Illuminate\Foundation\Events\Dispatchable;

abstract class JWTEvent
{
    use Dispatchable;
    
    /**
     * @param string $guard The authentication guard's name
     */
    public function __construct(
        public readonly string $guard
    ) {}
}
