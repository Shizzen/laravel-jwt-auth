<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Events;

class JWTAttempting extends JWTEvent
{
    /**
     * {@inheritdoc}
     * 
     * @param array<string, mixed> $credentials The credentials used to authenticate the user
     */
    public function __construct(
        string $guard,
        public readonly array $credentials
    ) {
        parent::__construct($guard);
    }
}
