<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Events;

use Shizzen\JWTAuth\Contracts\JWTSubject;

class JWTFailed extends JWTEvent
{
    /**
     * {@inheritdoc}
     * 
     * @param JWTSubject|null $subject The user (null if not found)
     * @param array<string, mixed> $credentials The credentials used to authenticate the user
     */
    public function __construct(
        string $guard,
        public readonly ?JWTSubject $subject,
        public readonly array $credentials
    ) {
        parent::__construct($guard);
    }
}
