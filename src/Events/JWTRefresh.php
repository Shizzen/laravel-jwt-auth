<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Events;

use Illuminate\Queue\SerializesModels;
use Shizzen\JWTAuth\Contracts\JWTSubject;
use Shizzen\JWTAuth\JWT;

class JWTRefresh extends JWTEvent
{
    use SerializesModels;

    /**
     * {@inheritdoc}
     * 
     * @param JWTSubject $subject The user
     */
    public function __construct(
        string $guard,
        public readonly JWTSubject $subject,
        public readonly JWT $oldJwt,
        public readonly JWT $newJwt
    ) {
        parent::__construct($guard);
    }
}
