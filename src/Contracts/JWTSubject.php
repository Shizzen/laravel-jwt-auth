<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Contracts;

use Illuminate\Contracts\Auth\Authenticatable;
use Shizzen\JWTAuth\JWTManager;

interface JWTSubject extends Authenticatable
{
    /**
     * Return a claim value from the subject.
     */
    public function getJWTClaim(string $claimName, JWTManager $manager): mixed;
}
