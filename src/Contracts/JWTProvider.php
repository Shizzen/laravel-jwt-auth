<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth\Contracts;

interface JWTProvider
{
    /**
     * Create a JSON Web Token.
     */
    public function encode(array $payload): string;

    /**
     * Decode a JSON Web Token.
     */
    public function decode(string $token): array;
}
