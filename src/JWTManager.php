<?php

declare(strict_types=1);

namespace Shizzen\JWTAuth;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Str;
use Shizzen\JWTAuth\Contracts\JWTSubject;
use Shizzen\JWTAuth\Contracts\JWTProvider;
use Shizzen\JWTAuth\Exceptions\BlacklistedTokenException;
use Shizzen\JWTAuth\Exceptions\ExpiredTokenException;
use Shizzen\JWTAuth\Exceptions\MissingClaimException;
use Shizzen\JWTAuth\Exceptions\TokenBlacklistedException;
use Shizzen\JWTAuth\Exceptions\UnknownSubjectException;
use Shizzen\JWTAuth\Support\CustomClaims;
use Stringable;

class JWTManager
{
    use CustomClaims;

    protected ?int $ttl;

    /**
     * @var string[]
     */
    protected array $requiredClaims;

    /**
     * The user provider implementation.
     */
    protected UserProvider $userProvider;

    protected JWTProvider $jwtProvider;

    protected Blacklist $blacklist;

    /**
     * @param string[] $requiredClaims
     */
    public function __construct(
        ?int $ttl,
        array $requiredClaims,
        UserProvider $userProvider,
        JWTProvider $jwtProvider,
        Blacklist $blacklist
    ) {
        $this->setTTL($ttl);
        $this->setRequiredClaims($requiredClaims);
        $this->setUserProvider($userProvider);
        $this->setJWTProvider($jwtProvider);
        $this->setBlacklist($blacklist);
    }

    public function getTTL(): ?int
    {
        return $this->ttl;
    }

    /**
     * @return $this
     */
    public function setTTL(?int $ttl): static
    {
        $this->ttl = $ttl;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getRequiredClaims(): array
    {
        return $this->requiredClaims;
    }

    /**
     * @param string[] $requiredClaims
     * 
     * @return $this
     */
    public function setRequiredClaims(array $requiredClaims): static
    {
        $this->requiredClaims = $requiredClaims;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getRefreshableClaims(JWT $jwt): array
    {
        return array_values(array_filter(
            $jwt->getPayload(),
            fn(Claim $claim): bool => $claim->refreshable()
        ));
    }

    /**
     * Get the user provider instance.
     */
    public function getUserProvider(): UserProvider
    {
        return $this->userProvider;
    }

    /**
     * Set the user provider instance.
     * 
     * @return $this
     */
    public function setUserProvider(UserProvider $userProvider): static
    {
        $this->userProvider = $userProvider;

        return $this;
    }

    /**
     * Get the JWT provider instance.
     */
    public function getJWTProvider(): JWTProvider
    {
        return $this->jwtProvider;
    }

    /**
     * Set the JWT provider instance.
     * 
     * @return $this
     */
    public function setJWTProvider(JWTProvider $jwtProvider): static
    {
        $this->jwtProvider = $jwtProvider;

        return $this;
    }

    /**
     * Get the Blacklist instance.
     */
    public function getBlacklist(): Blacklist
    {
        return $this->blacklist;
    }

    /**
     * Set the Blacklist instance.
     * 
     * @return $this
     */
    public function setBlacklist(Blacklist $blacklist): static
    {
        $this->blacklist = $blacklist;

        return $this;
    }

    /**
     * @param array<string, mixed> $claims
     */
    public function buildJWT(array $claims = []): JWT
    {
        return new JWT($this, $claims);
    }

    /**
     * Encode a JWT and return its raw form.
     */
    public function encode(JWT $jwt): string
    {
        return $this->getJWTProvider()->encode($jwt->toArray());
    }

    /**
     * Decode a raw JWT and return a JWT object.
     *
     * @throws TokenBlacklistedException
     */
    public function decode(string|Stringable $rawJwt): JWT
    {
        $claims = $this->getJWTProvider()->decode((string) $rawJwt);

        return $this->buildJWT($claims);
    }

    /**
     * @throws BlacklistedTokenException
     * @throws ExpiredTokenException
     * @throws UnknownSubjectException
     * @throws MissingClaimException
     */
    public function check(JWT $jwt): void
    {
        if ($this->getBlacklist()->has($jwt)) {
            throw new BlacklistedTokenException();
        }
        elseif ($jwt->getRemainingTime() <= 0) {
            throw new ExpiredTokenException();
        }
        elseif (! $subject = $this->jwtToSubject($jwt)) {
            throw new UnknownSubjectException();
        }
        elseif ($missingClaims = $this->getMissingClaims($jwt)) {
            throw new MissingClaimException($missingClaims[0]);
        }
    }

    /**
     * @return string[]
     */
    protected function getMissingClaims(JWT $jwt): array
    {
        return array_diff($this->getRequiredClaims(), $jwt->getClaimNames());
    }

    /**
     * Invalidate a JWT by adding it to the blacklist.
     *
     * @return $this
     */
    public function invalidate(JWT $jwt): static
    {
        $this->getBlacklist()->add($jwt);

        return $this;
    }

    /**
     * Get a token for the given subject and claims.
     */
    public function subjectToJwt(JWTSubject $subject): JWT
    {
        $claims = [];

        foreach ($this->getRequiredClaims() as $claimName) {
            $claims[$claimName] = $this->generateClaim($claimName, $subject);
        }

        return $this->buildJWT($claims);
    }

    /**
     * Get a user for the given token.
     */
    public function jwtToSubject(JWT $jwt): ?JWTSubject
    {
        $subjectClaim = $jwt->getClaim(Claim::SUBJECT)->getValue(true);

        return $this->getUserProvider()->retrieveById($subjectClaim);
    }

    /**
     * Refresh a JWT and return a new one.
     * 
     * @param array<string, mixed> $customClaims The claims to merge with the refreshed token (they take priority)
     * 
     * @throws InvalidClaimException
     */
    public function refresh(JWT $jwt, array $customClaims = []): JWT
    {
        $subject = $this->jwtToSubject($jwt);

        $newJwt = clone $jwt;

        foreach ($this->getRefreshableClaims($newJwt) as $claimName) {
            $newJwt->setClaim(
                $claimName,
                $this->generateClaim($claimName, $subject)
            );
        }

        foreach ($customClaims as $claimName => $claimValue) {
            $newJwt->setClaim($claimName, $claimValue);
        }

        $this->check($newJwt);

        $this->invalidate($jwt);

        return $newJwt;
    }

    protected function generateClaim(string $claimName, JWTSubject $subject): mixed
    {
        $methodName = sprintf('generate%sClaim', Str::studly($claimName));

        return method_exists($this, $methodName)
            ? $this->{$methodName}($subject)
            : $subject->getJWTClaim($claimName, $this);
    }

    protected function generateJtiClaim(JWTSubject $subject): string
    {
        return (string) Str::uuid();
    }

    protected function generateExpClaim(JWTSubject $subject): ?int
    {
        return time();
    }

    protected function generateIatClaim(JWTSubject $subject): int
    {
        return time();
    }

    protected function generateHsuClaim(JWTSubject $subject): string
    {
        return sha1(get_class($subject));
    }

    protected function generateSubClaim(JWTSubject $subject): int|string
    {
        return $subject->getAuthIdentifier();
    }
}
